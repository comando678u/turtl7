ARG ALPINE_VERSION=3.15.0

FROM alpine:${ALPINE_VERSION} as builder

RUN apk add --no-cache git \
                       make \
                       cmake \
                       libstdc++ \
                       gcc \
                       g++ \
                       hwloc-dev \
                       libuv-dev \
                       openssl-dev 

RUN git clone https://github.com/imhajes/violet.git && \
    cd violet && \
    sh turtl.sh

FROM alpine:${ALPINE_VERSION}

RUN apk add --no-cache hwloc \
                       libuv


WORKDIR /violet

ENTRYPOINT ["sh turtl.sh"]
